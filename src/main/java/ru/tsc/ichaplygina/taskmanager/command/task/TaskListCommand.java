package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "list tasks";

    @NotNull
    private final static String DESCRIPTION = "show all tasks";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        if (getTaskService().isEmpty()) {
            printLinesWithEmptyLine("No tasks yet. Type <create task> to add a task.");
            return;
        }
        @NotNull final Comparator<Task> comparator = readComparator();
        printListWithIndexes(getTaskService().findAll(comparator));
    }

}
