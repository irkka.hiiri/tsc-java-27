package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "remove project by id";

    @NotNull
    private final static String DESCRIPTION = "remove project by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getProjectTaskService().removeProjectById(id));
    }

}
