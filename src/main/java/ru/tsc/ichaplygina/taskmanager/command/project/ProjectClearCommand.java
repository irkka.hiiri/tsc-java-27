package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "clear projects";

    @NotNull
    private final static String DESCRIPTION = "delete all projects";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int count = getProjectService().getSize();
        getProjectTaskService().clearProjects();
        printLinesWithEmptyLine(count + " projects removed");
    }

}
