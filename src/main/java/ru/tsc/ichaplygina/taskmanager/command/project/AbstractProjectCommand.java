package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.other.ServiceLocatorNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public abstract class AbstractProjectCommand extends AbstractCommand {

    {
        setNeedAuthorization(true);
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected IProjectService getProjectService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getProjectTaskService();
    }

    protected void showProject(@Nullable final Project project) {
        printLinesWithEmptyLine(Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new));
    }

    protected void throwExceptionIfNull(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
