package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class UserShowByIdCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "show user by id";

    @NotNull
    private static final String DESCRIPTION = "show user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @Nullable final User user = getUserService().findById(id);
        showUser(user);
    }

}
