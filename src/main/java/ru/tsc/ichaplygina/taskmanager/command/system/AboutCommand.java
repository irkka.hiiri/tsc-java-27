package ru.tsc.ichaplygina.taskmanager.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "about";

    @NotNull
    private final static String ARG_NAME = "-a";

    @NotNull
    private final static String DESCRIPTION = "show developer info";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        printLinesWithEmptyLine(Manifests.read("developer"), Manifests.read("email"));
    }

}
