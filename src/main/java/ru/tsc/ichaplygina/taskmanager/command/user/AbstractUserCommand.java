package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.other.ServiceLocatorNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected final String ENTER_LOGIN = "Enter login: ";

    @NotNull
    protected final String ENTER_PASSWORD = "Enter password: ";

    @NotNull
    protected final String ENTER_EMAIL = "Enter email: ";

    @NotNull
    protected final String ENTER_ROLE = "Enter user role: (default: User) ";

    @NotNull
    protected final String ENTER_FIRST_NAME = "Enter first name: ";

    @NotNull
    protected final String ENTER_MIDDLE_NAME = "Enter middle name: ";

    @NotNull
    protected final String ENTER_LAST_NAME = "Enter last name: ";

    @NotNull
    protected final String USER_ALREADY_LOCKED = "User already locked. ";

    @NotNull
    protected final String USER_IS_NOT_LOCKED = "User is not locked. ";

    {
        setNeedAuthorization(true);
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected IUserService getUserService() {
        if (!Optional.ofNullable(serviceLocator).isPresent()) throw new ServiceLocatorNotFoundException();
        return serviceLocator.getUserService();
    }

    protected void showUser(@Nullable final User user) {
        printLinesWithEmptyLine(Optional.ofNullable(user).orElseThrow(UserNotFoundException::new));
    }

    protected void throwExceptionIfNull(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
    }

}
