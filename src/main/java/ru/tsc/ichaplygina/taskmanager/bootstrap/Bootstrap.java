package ru.tsc.ichaplygina.taskmanager.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.api.repository.*;
import ru.tsc.ichaplygina.taskmanager.api.service.*;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.comparator.CommandComparator;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandDescriptionEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandNameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.repository.*;
import ru.tsc.ichaplygina.taskmanager.service.*;
import ru.tsc.ichaplygina.taskmanager.util.SystemUtil;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
public final class Bootstrap implements ServiceLocator {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    @NotNull
    private static final String PID_FILE_NAME = "task-manager.pid";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService, propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, authService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, authService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskService, projectService, authService);

    public void executeCommand(@NotNull final String commandName) {
        if (isEmptyString(commandName)) return;
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getCommands().get(commandName))
                .orElseThrow(() -> new CommandNotFoundException(commandName));
        execute(command);
    }

    public void executeCommandByArgument(@NotNull final String argument) {
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getArguments().get(argument))
                .orElseThrow(() -> new CommandNotFoundException(argument));
        execute(command);
    }

    public void execute(@NotNull final AbstractCommand command) {
        if (command.getNeedAuthorization()) authService.throwExceptionIfNotAuthorized();
        authService.checkRoles(command.getRoles());
        command.execute();
    }

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.ichaplygina.taskmanager.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.ichaplygina.taskmanager.command.AbstractCommand.class)
                .stream()
                .sorted(CommandComparator.getInstance())
                .collect(Collectors.toList());
        boolean isAbstract;
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registerCommand(clazz.newInstance());
        }
    }

    private void initData() {
        try {
            userService.add("root", "toor", "root@domain", Role.ADMIN, "Root", "Root", "Root");
            userService.add("vasya", "123", "vasya@domain", Role.USER, "Vassily", "Ivanovich", "Pupkin");
            userService.add("vova", "123", "vova@domain", Role.USER, "Vova", "V.", "P.");
            authService.login("root", "toor");
            taskService.add("en", "Sample Task");
            taskService.add("to", "Sample Task");
            taskService.add("tre", "Sample Task");
            taskService.add("fire", "Sample Task");
            taskService.add("fem", "Sample Task");
            projectService.add("Yksi", "Sample Project");
            projectService.add("Kaksi", "Sample Project");
            projectService.add("Kolme", "Sample Project");
            projectService.add("Nelja", "Sample Project");
            projectService.add("Viisi", "Sample Project");
            authService.logout();
            authService.login("vasya", "123");
            taskService.add("vasya task 1", "Sample Task");
            taskService.add("vasya task 2", "Sample Task");
            projectService.add("Vasya project 1", "Sample Project");
            projectService.add("Vasya project 2", "Sample Project");
            authService.logout();
            authService.login("vova", "123");
            taskService.add("vova task 1", "Sample Task");
            taskService.add("vova task 2", "Sample Task");
            projectService.add("vova project 1", "Sample Project");
            projectService.add("vova project 2", "Sample Project");
            authService.logout();
        } catch (@NotNull final AbstractException e) {
            logService.error(e);
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILE_NAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILE_NAME);
        file.deleteOnExit();
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        initData();
        showWelcome();
        @NotNull String command = readCommand();
        while (true) {
            try {
                if (isEmptyString(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                System.out.println(APP_COMMAND_SUCCESS);
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    @NotNull
    public String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void registerCommand(@NotNull final AbstractCommand command) {
        try {
            @NotNull final String terminalCommand = command.getCommand();
            @NotNull final String commandDescription = command.getDescription();
            @Nullable final String commandArgument = command.getArgument();
            if (isEmptyString(terminalCommand)) throw new CommandNameEmptyException();
            if (isEmptyString(commandDescription)) throw new CommandDescriptionEmptyException();
            command.setServiceLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (isEmptyString(commandArgument)) return;
            commandService.getArguments().put(commandArgument, command);
        } catch (@NotNull AbstractException e) {
            logService.error(e);
        }
    }

    public void run(@NotNull final String... args) {
        initPID();
        initCommands();
        if (args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommandByArgument(args[0]);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

}
