package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.api.IService;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(@NotNull final E entity) {
        repository.add(entity);
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        repository.addAll(entities);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    public boolean isEmpty() {
        return repository.isEmpty();
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.removeById(id);
    }

}
