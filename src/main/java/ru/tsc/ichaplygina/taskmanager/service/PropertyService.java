package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.property.IApplicationProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IPasswordProperty;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService, IPasswordProperty, IApplicationProperty {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_PROPERTY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_JAVA_OPTION = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_ENVIRONMENT_VARIABLE = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT_VALUE = "";

    @NotNull
    private static final String PASSWORD_ITERATION_PROPERTY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_JAVA_OPTION = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_ENVIRONMENT_VARIABLE = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT_VALUE = "0";

    @NotNull
    private static final String APP_VERSION_PROPERTY = "application.version";

    @NotNull
    private static final String APP_VERSION_JAVA_OPTION = "APPLICATION_VERSION";

    @NotNull
    private static final String APP_VERSION_ENVIRONMENT_VARIABLE = "APPLICATION_VERSION";

    @NotNull
    private static final String APP_VERSION_DEFAULT_VALUE = "";

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        loadPropertiesFromFile();
    }

    @SneakyThrows
    private void loadPropertiesFromFile() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getValue(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                            @NotNull final String nameProperty, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(nameJavaOpts)) return System.getProperty(nameJavaOpts);
        if (System.getenv().containsKey(nameEnvironmentVariable)) return System.getenv(nameEnvironmentVariable);
        if (properties.containsKey(nameProperty)) return properties.getProperty(nameProperty);
        return defaultValue;
    }

    @NotNull
    private Integer getValueInteger(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                                    @NotNull final String nameProperty, @NotNull final String defaultValue) {
        @NotNull final String value = getValue(nameJavaOpts, nameEnvironmentVariable, nameProperty, defaultValue);
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final NumberFormatException e) {
            return Integer.parseInt(PASSWORD_ITERATION_DEFAULT_VALUE);
        }
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue(APP_VERSION_JAVA_OPTION, APP_VERSION_ENVIRONMENT_VARIABLE, APP_VERSION_PROPERTY, APP_VERSION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInteger(PASSWORD_ITERATION_JAVA_OPTION,
                PASSWORD_ITERATION_ENVIRONMENT_VARIABLE,
                PASSWORD_ITERATION_PROPERTY,
                PASSWORD_ITERATION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_JAVA_OPTION,
                PASSWORD_SECRET_ENVIRONMENT_VARIABLE,
                PASSWORD_SECRET_PROPERTY,
                PASSWORD_SECRET_DEFAULT_VALUE);
    }

}
