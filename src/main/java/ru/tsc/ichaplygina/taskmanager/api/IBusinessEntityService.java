package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityService<E extends AbstractBusinessEntity> extends IService<E> {

    void add(@NotNull String name, @Nullable String description);

    @Nullable E completeById(@NotNull String id);

    @Nullable E completeByIndex(int index);

    @Nullable E completeByName(@NotNull String name);

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @Nullable
    E findByIndex(int index);

    @Nullable
    E findByName(@NotNull String name);

    @Nullable
    String getId(int index);

    @Nullable
    String getId(@NotNull String name);

    boolean isNotFoundById(@NotNull String id);

    @Nullable
    E removeByIndex(int index);

    @Nullable
    E removeByName(@NotNull String name);

    @Nullable
    E startById(@NotNull String id);

    @Nullable
    E startByIndex(int index);

    @Nullable
    E startByName(@NotNull String name);

    @Nullable
    E updateById(@NotNull String id, @NotNull String name, @Nullable String description);

    @Nullable
    E updateByIndex(int index, @NotNull String name, @Nullable String description);

    @Nullable
    E updateStatusById(@NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIndex(int index, @NotNull Status status);

    @Nullable
    E updateStatusByName(@NotNull String name, @NotNull Status status);

}
