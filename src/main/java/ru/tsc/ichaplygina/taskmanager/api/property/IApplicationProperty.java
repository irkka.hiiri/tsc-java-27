package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IApplicationProperty {
    @NotNull String getApplicationVersion();
}
