package ru.tsc.ichaplygina.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessEntityRepository<Task> {

    @Nullable Task addTaskToProject(@NotNull String taskId, @NotNull String projectId);

    @Nullable Task addTaskToProjectForUser(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

    @NotNull List<Task> findAllByProjectId(@NotNull String projectId);

    @NotNull List<Task> findAllByProjectId(@NotNull String projectId, @NotNull Comparator<Task> comparator);

    @NotNull List<Task> findAllByProjectIdForUser(@NotNull String userId, @NotNull String projectId);

    @NotNull List<Task> findAllByProjectIdForUser(@NotNull String userId, @NotNull String projectId, @NotNull Comparator<Task> comparator);

    boolean isNotFoundTaskInProject(@NotNull String taskId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String projectId);

    @Nullable Task removeTaskFromProject(@NotNull String taskId, @NotNull String projectId);

    @Nullable Task removeTaskFromProjectForUser(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

}
