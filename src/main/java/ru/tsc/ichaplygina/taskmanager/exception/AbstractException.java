package ru.tsc.ichaplygina.taskmanager.exception;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractException extends RuntimeException {

    public AbstractException(@Nullable final String message) {
        super(message);
    }

}
